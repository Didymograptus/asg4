**AGS Data Format Working Group (DFWG)**  
**Website proposal**

**Platform:**  
1. Use AGS WordPress(WP) web site and theme.  
2. Use AGS WordPress User list.*

**Functionality required:**  
1. Static web pages (standard WP)  
2. News/Blog pages (standard WP)  
3. Tables of data format fields/data (COTS* plugin - 'wpDatatables' -approx. £30 - one time)  
4. Discussion Forum (COTS plugin - 'wpFORO' - free)  
5. Contact Forms (COTS plugin - 'Ninja Forms' - free)  
6. User access levels.

**Web site development**  
1. All development to be undertaken using resource on the DFWG  
2. Consultancy support to be provided by ????? at a cost of £??.??  
3. Existing content to be migrated to the new platform.  
4. New content (AGS 4.0.4) to be managed as set out below  

**Website mangement:**  
1. All content to be updated by DFWG  
2. Admin access to AGS main site Wordpress Dashboard required by named members of the AGS DFWG.  
3. Version control of content (incl. AGS format) to be managed using distributed system (GitLab) (free).  
4. Web site content to be subject to formal approval process (managed using GitLab)  
5. Content linked directly to data held on GitLab to ensure "one version of the truth" and allow for easy updates.  
6. Issues to be managed using GitLab - email direct from Ninja Forms or direct entry  

**Notes:**  
*User access levels to be reviewed against main AGS requirements.  
**COTS = Commercial Off The Shelf Software  

This README would normally document whatever steps are necessary to get your application up and running.